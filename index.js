import React, {useCallback, useEffect, useReducer} from 'react'
import { runSaga, stdChannel } from 'redux-saga'
import EventEmitter from 'events'

/**
 * Use duck react hook
 *
 * Import reducer and actionCreators from a 'duck file', pass them to this hook,
 * and use the returned state and boundActions (actionCreators wrapped in a 
 * dispatch function) inside a react component as you like.
 *
 * @see https://github.com/erikras/ducks-modular-redux
 */
export default function useDuck(reducer, actions, initialState={}) {
  const reducerArgs = [reducer, initialState]
  if (reducer.init) {
    reducerArgs.push(reducer.init)
  }
  const [state, dispatch] = useReducer(...reducerArgs)
  const [boundActions, setBoundActions] = useState(null)

  if (reducer.saga) {
    useSaga(
      reducer.saga,
      state,
      // connect the saga event emitter
      (emitter) => {
        // whenever an action is emitted (i.e. on put), we call dispatch
        emitter.on('action', dispatch)
        setBoundActions(createActions(
          actions,
          dispatch,
          // whenever we dispatch an action, we emit it to the saga
          (action => emitter.emit('action', action))
        ))
      }
    )
  } else {
    useCallback(
      () => setBoundActions(createActions(actions, dispatch)),
      [actions]
    )
  }

  return [state, boundActions]
}

/**
 * Use saga react hook.
 *
 * Oh my! A hook to use saga's outside redux.
 */
export function useSaga(saga, state, onReady) {
  const stateRef = useRef(null)
  stateRef.current = state
  useEffect(
    () => {
      const emitter = new EventEmitter()
      const channel = stdChannel()

      emitter.on("action", channel.put)

      const task = runSaga({
        // this will be used to orchestrate take and put Effects
        channel,
        // this will be used to resolve put Effects
        dispatch(output) {
          emitter.emit("action", output)
        },
        // this will be used to resolve select Effects
        getState() {
          return stateRef.current
        }
      }, saga)

      onReady(emitter)

      // cleanup
      return () => {
        task.cancel()
      }
    },
    [saga]
  )
}

/**
 * Utility function like redux' bindActionCreators
 */
export function createActions(actionCreators, dispatch, beforeDispatch=null, afterDispatch=null) {
  return Object.keys(actionCreators).reduce(
    (acc, actionName) => {
      acc[actionName] = (...args) => {
        const action = actionCreators[actionName](...args)
        if (beforeDispatch) beforeDispatch(action)
        dispatch(action)
        if (afterDispatch) afterDispatch(action)
      }
      return acc
    }, {}
  )
}