import { call, delay, put, takeLatest } from 'redux-saga/effects'

/**
 * A duck (reducer) interface
 *
 * @see https://github.com/erikras/ducks-modular-redux
 */

const ns = 'example/audio'

// actions

const SET_READYSTATE    = `${ns}/CHANGE_READYSTATE`
const MUTE              = `${ns}/MUTE`
const UNMUTE            = `${ns}/UNMUTE`
const SET_VOLUME        = `${ns}/SET_VOLUME`
const FADE_VOLUME       = `${ns}/FADE_VOLUME`
const FADE_VOLUME_START = `${ns}/FADE_VOLUME_START`
const FADE_VOLUME_END   = `${ns}/FADE_VOLUME_END`

// reducer
export default function reducer(state, action) {
  switch (action.type) {

    case SET_READYSTATE:
      return {...state, readyState: action.readyState}

    // audio
    case MUTE:
      return {...state, muted: true}
    case UNMUTE:
      return {...state, muted: false}

    case FADE_VOLUME_START:
      return {...state, volumeFadeInProgress: true}
    case FADE_VOLUME_END:
      return {...state, volumeFadeInProgress: false}
    case SET_VOLUME:
      return {...state, volume: action.volume}

    default:
      return state
  }
}

// initial state
reducer.init = (initialState={}) => {
  const initialStateValues = (
    Object
      .keys(initialState)
      .filter(key => typeof initialState[key] !== 'undefined')
      .reduce(
        (acc, key) => {
          acc[key] = initialState[key]
          return acc
        }, {}
      )
  )
  return {
    readyState: 0,
    playing: false,
    volume: 0,
    volumeFadeInProgress: false,
    muted: false,
    ...initialStateValues
  }
}

// Action creators

export function setReadyState(readyState) {
  return { type: SET_READYSTATE, readyState }
}

export function mute() {
  return { type: MUTE }
}

export function unmute() {
  return { type: UNMUTE }
}

export function setVolume(volume) {
  return { type: SET_VOLUME, volume }
}

export function fadeVolume(volumeStart, volumeEnd, durationInMs=1000) {
  return {
    type: FADE_VOLUME,
    durationInMs,
    volumeStart,
    volumeEnd
  }
}

export function fadeInVolume(
  durationInMs=1000,
  volumeEnd=1,
  volumeStart=0,
) {
  return fadeVolume(volumeStart, volumeEnd, durationInMs)
}

export function fadeOutVolume(
  durationInMs=1000,
  volumeEnd=0,
  volumeStart=1,
) {
  return fadeVolume(volumeStart, volumeEnd, durationInMs)
}

// Side effects (saga's / flows)

function* fadeVolumeLoop({volumeStart, volumeEnd, durationInMs=2000}) {
  yield put({type: FADE_VOLUME_START})

  const fadeOut = volumeEnd < volumeStart
  const delta = volumeEnd-volumeStart

  let starttime = -1
  let timestamp = -1
  let timeElapsed, progress, volume

  while (timestamp < starttime+durationInMs) {
    timestamp = Date.now()
    if (starttime < 0) starttime = timestamp
    timeElapsed = timestamp-starttime
    progress = timeElapsed/durationInMs
    volume = parseFloat((volumeStart+progress*delta).toFixed(7))
    if ((fadeOut && volume < volumeEnd) || (!fadeOut && volume > volumeEnd)) {
      volume = volumeEnd
    }
    yield put(setVolume(volume))
    // yield call(delay, 100) // @TODO check if this can be made to work
    yield delay(100)
  }

  yield put({type: FADE_VOLUME_START})
}

reducer.saga = function* audioSaga() {
  yield takeLatest(FADE_VOLUME, fadeVolumeLoop)
}