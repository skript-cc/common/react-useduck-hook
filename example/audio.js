import React, {useRef} from 'react'
import useDuck from '../'
import reducer, * as actionCreators from './audio-duck.js'

export default function Audio({
  src,
  muted,
  volume,
  autoPlay=false,
  loop=false,
  playerRef=null
}) {
  const mediaRef = useRef(null)
  const stateRef = useRef(null)
  const [state, actions] = useDuck(reducer, actionCreators, {volume, muted})

  stateRef.current = state

  // setup / connect media element
  useEffect(
    () => {
      if (!mediaRef.current || !actions) return

      const mediaEl = mediaRef.current

      // expose a public interface
      if (playerRef) {
        playerRef.current = {
          getState: () => stateRef.current,
          actions: {
            ...actions,
            play: () => mediaEl.play(),
            pause: () => mediaEl.pause()
          }
        }
      }

      const onLoadedData = e => { actions.setReadyState(mediaEl.readyState) }
      mediaEl.addEventListener('loadeddata', onLoadedData)

      // cleanup
      return () => {
        mediaEl.removeEventListener('loadeddata', onLoadedData)
      }
    },
    [mediaRef, actions]
  )

  // update audio values
  useEffect(
    () => {
      const mediaEl = mediaRef.current
      mediaEl.volume = state.volume
      mediaEl.muted = state.muted
    },
    [state.volume, state.muted]
  )

  return (
    <audio
      ref={mediaRef}
      autoPlay={autoPlay}
      loop={false}
      src={src}
      muted={state.muted}
      preload='auto'
    />
  )
}